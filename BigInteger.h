#include <iostream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;
using std::swap;

class BigInteger{
	public:
		BigInteger() {
			d.resize(32,0);
		}
		~BigInteger () {
			d.clear();
		}
		BigInteger(int input, int width = 32) {
			d.resize(width);
			setValue(input);
		}
		//copy constructor
		BigInteger(BigInteger& number){
			d = number.d;
		}
		//move constructor
		BigInteger(BigInteger&& number) : BigInteger() {
			swap(*this, number);
		}
		//assign constructor
		BigInteger& operator=(BigInteger number) {
			
			d.swap(number.d);
			return *this;
		}		
		BigInteger operator-();
		BigInteger operator+(const BigInteger& rhs) const;
		BigInteger& operator+=(const BigInteger& rhs);
		BigInteger operator-(BigInteger& rhs);
    	BigInteger operator<<(const int nbit) const;
    	BigInteger operator>>(const int nbit) const;
    	BigInteger positiveProd(BigInteger& rhs);
    	BigInteger operator*(BigInteger& rhs);
    	BigInteger operator/(BigInteger& rhs);
    	BigInteger operator%(BigInteger& rhs);
    	bool operator<(BigInteger& rhs);
    	friend ostream& operator << (ostream& os, BigInteger &number);
    	friend istream& operator >> (istream& is, BigInteger &number);
    	
    	inline bool isNegative() const {return d[d.size()-1];}
    	
    	inline int getMSB(int hint) { // get most significant bit
    		for (int i = hint; i >= 0; i--)
    		{
    			if (d[i]) return i;
    		}
    		return -1;
    	}
    	inline int getInt(int hint) {
    		int rs = 0;
    		for (int i = 0; i < hint; ++i)
    		{
    			if (d[i]) rs += d[i]<<i;
    		}
    		return rs;
    	}
    	string to_string();
	private:
		void setValue(int input);
		vector<bool> d; // ordered as Big Endian		
};
void BigInteger::setValue(int input){
	if (input > 0) {
		int a = input;
		for(int i = 0; i < d.size();i++,a>0) {
			d[i] = a%2;
			a = (a - d[i])/2;
		}
	}else {
		int a = -input;
		int width = d.size();
		for(int i = 0; i < width;i++,a>0) {
			d[i] = a%2;
			a = (a - d[i])/2;
		}
		bool sw = false;
		for(int i = 0; i < width; i++) {
			if (sw) {
				this->d[i] = !this->d[i];
				continue;
			} 
			if (this->d[i]) sw= true;
		}
	} 
}
BigInteger BigInteger::operator-(){
	int width =  this->d.size();
	BigInteger rs = *this;
	bool sw = false;
	for(int i = 0; i < width; i++) {
		if (sw) {
			rs.d[i] = !this->d[i];
			continue;
		} 
		if (this->d[i]) sw= true;
	}
	return rs;
}
BigInteger BigInteger::operator+(const BigInteger& rhs) const {
	int width =  this->d.size();
	BigInteger rs(0,width);
	bool carry = 0;
	for(int i = 0; i < width; i++) {
		rs.d[i] = rhs.d[i] ^ d[i] ^ carry;
		carry = ((rhs.d[i] & d[i]) | carry) & (rhs.d[i] | d[i]);
	}
	return rs;	
}
BigInteger& BigInteger::operator+=(const BigInteger& rhs){
	bool carry = 0;
	for(int i = 0; i < d.size(); i++) {
		bool tmp = rhs.d[i] ^ d[i] ^ carry;
		carry = ((rhs.d[i] & d[i]) | carry) & (rhs.d[i] | d[i]);
		d[i] = tmp;
	}
	return *this;
}

BigInteger BigInteger::operator-(BigInteger& rhs){
	return *this + (-rhs);
}

BigInteger BigInteger::operator<<(const int nbit) const{
	int width =  this->d.size();
	BigInteger rs(0,width);
	for(int i = nbit ; i < width ; i++) {
		rs.d[i] = this->d[i-nbit];
	}
	return rs;
}

BigInteger BigInteger::positiveProd(BigInteger& rhs){
	int width =  this->d.size();
	BigInteger rs(0,width);
	for(int i = 0; i < width; i++) {
		if (d[i]) {
			rs += rhs<<i; 
		}
	}
	return rs;
}
BigInteger BigInteger::operator*(BigInteger& rhs){
	if (this->isNegative() && rhs.isNegative()) {
		BigInteger a = -(*this);
		BigInteger b = -rhs;
		return a.positiveProd(b);
	}
	if (this->isNegative()) {
		return -(-(*this)).positiveProd(rhs);	
	}
	if (rhs.isNegative()) {
		BigInteger b = -rhs;
		return -(positiveProd(b));
	}
	return positiveProd(rhs);
}
BigInteger BigInteger::operator/(BigInteger& rhs){
	int width = this->d.size();
	BigInteger rs(0,width);
	int s1 = getMSB(width-1);
	int s2 = rhs.getMSB(width-1);
	BigInteger a = *this;
	int diff = s1 - s2;
	if (diff < 0) return rs;
	do {
		diff = s1 - s2;	
		if (diff == 0 ) {
			if (a < rhs) return rs;
		}
		a += -rhs<<diff;
		if (a.isNegative()) {
			diff = diff - 1;
			a += rhs<<diff;
		}
		rs.d[diff] = 1;		  		
		s1 = a.getMSB(s1);
	} while (s1 >= s2);
	return rs;
}
BigInteger BigInteger::operator%(BigInteger& rhs){
	BigInteger a = *this;
	if (a < rhs) return a;
	int width = this->d.size();
	int s1 = getMSB(width-1);
	int s2 = rhs.getMSB(width-1);
	int diff;
	do {
		diff = s1 - s2;
		if (diff == 0 ) {
			if (a < rhs) return a;
		}
		a += -rhs<<diff;
		if (a.isNegative()) {
			diff = diff - 1;
			a += rhs<<diff;
		}		  		
		s1 = a.getMSB(s1);
	} while (s1 >= s2);
	return a;
}

bool BigInteger::operator<(BigInteger& rhs){
	BigInteger a = *this - rhs;
	return a.isNegative();
}

inline ostream& operator << (ostream& os, BigInteger &number){
	os << number.to_string();
	return os;
}
inline istream& operator >> (istream& is, BigInteger &number){
	return is;
}


string BigInteger::to_string(){
	int width = d.size();
	if (!d[width-1]) {
		ostringstream oss;
		int buf[100];
		BigInteger a = *this;
		BigInteger b(10, width);
		int i = 0;
		int msb = a.getMSB(width);
		while (msb >= 0) {
			BigInteger c = a%b;
			BigInteger tmp = (a-c)/b;
			a = tmp;
			buf[i++] = c.getInt(4);
			msb = a.getMSB(msb); 			
		} 
		for (int j = i-1; j >=0; j--)
		{
			oss << buf[j];
		}
		return oss.str();
	}
	else{
		BigInteger neg = -(*this);
		return "-"+neg.to_string();
	}	
}