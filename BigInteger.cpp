#include "BigInteger.h"
#include <cassert>

void ProductTest() {
	BigInteger a1(82351231,128);
	BigInteger a2(12231412,128);
	BigInteger a3(121345, 128);
	BigInteger a4 = a1*a2;
	BigInteger a5 = a4*a3;
	assert(a5.to_string() == "122227400826347331340" && "ProductTest not passed");
}

void FibonacciTest() {
	BigInteger a(1,128);
	BigInteger b(1,128);
	for(int i = 0; i < 100; i++) {
		BigInteger tmp = b;
		b += a;
		a = tmp;
	}
	assert(b.to_string() == "927372692193078999176" && "FibonacciTest not passed");
}

void HackerrankTest() {
	BigInteger a(0,128);
	BigInteger b(1,128);
	for(int i = 0; i < 8; i++) {
		BigInteger c = b*b + a;
		a = b;
		b = c;
	}
	assert(b.to_string() == "84266613096281243382112" && "HackerrankTest not passed");
}

int main() {
	ProductTest();
	FibonacciTest();
	HackerrankTest();
	return 0;
}